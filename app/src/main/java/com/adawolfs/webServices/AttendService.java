package com.adawolfs.webServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by adawolfs on 4/28/16.
 */
public class AttendService extends MainService {
    static String url = "api/attends/";

    public static String getHistorial(String user) {
        String params = user;
        String json = (String) getRequest(url, params, true);
        return json;
    }
}
