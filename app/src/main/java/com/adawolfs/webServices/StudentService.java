package com.adawolfs.webServices;

/**
 * Created by adawolfs on 4/28/16.
 */
public class StudentService extends MainService {
    static String url = "api/students/";
    public static final String LOCATE = "LOCATE";
    public static final String ATTEND = "ATTEND";
    private static String operation;

    public static String getStudents() {
        String params = getUser();
        String json = (String) getRequest(url, params, true);
        return json;
    }

    public static String getOperation() {
        return operation;
    }

    public static void setOperation(String operation) {
        StudentService.operation = operation;
    }
}
