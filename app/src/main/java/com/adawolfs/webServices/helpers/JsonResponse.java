package com.adawolfs.webServices.helpers;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.LayeredSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by adawolfs on 29/04/16.
 */
public class JsonResponse {


    /**
     * Producción
     */
    static HttpClient client = HttpClientBuilder.create().build();

    public static String getString(HttpRequestBase request){

        String result = null;
        HttpResponse response;

        try {
            response = client.execute(request);
            Log.i("Inicial Json", response.getStatusLine().toString());

            HttpEntity entity = response.getEntity();
            if(entity != null){
                InputStream instream = entity.getContent();
                result = convertStreamToString(instream);
                Log.i("Result Stream", result);
                Log.i("Final Json", "<jsonobject>\n" + result + "\n</jsonobject>");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static JSONObject getJsonObject(HttpRequestBase request){

        JSONObject json = null;
        HttpResponse response;

        try {
            response = client.execute(request);
            Log.i("Inicial Json", response.getStatusLine().toString());

            HttpEntity entity = response.getEntity();
            if(entity != null){
                InputStream instream = entity.getContent();
                String result = convertStreamToString(instream);
                Log.i("Result Stream", result);
                json = new JSONObject(result);
                Log.i("Final Json", "<jsonobject>\n" + json.toString() + "\n</jsonobject>");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json;
    }

    private static void workAroundReverseDnsBugInHoneycombAndEarlier(HttpClient client) {
        // Android had a bug where HTTPS made reverse DNS lookups (fixed in Ice Cream Sandwich)
        // http://code.google.com/p/android/issues/detail?id=13117
        SocketFactory socketFactory = new LayeredSocketFactory() {

            SSLSocketFactory delegate = SSLSocketFactory.getSocketFactory();
            @Override
            public Socket createSocket() throws IOException {
                return delegate.createSocket();
            }
            @Override
            public Socket connectSocket(Socket sock, String host, int port, InetAddress localAddress, int localPort, HttpParams params)
                    throws IOException, UnknownHostException,
                    ConnectTimeoutException {
                // TODO Auto-generated method stub
                return delegate.connectSocket(sock, host, port, localAddress, localPort, params);
            }
            @Override
            public boolean isSecure(Socket sock) throws IllegalArgumentException {
                return delegate.isSecure(sock);
            }
            @Override
            public Socket createSocket(Socket socket, String host, int port,
                                       boolean autoClose) throws IOException {
                injectHostname(socket, host);
                return delegate.createSocket(socket, host, port, autoClose);
            }
            private void injectHostname(Socket socket, String host) {
                try {
                    Field field = InetAddress.class.getDeclaredField("hostName");
                    field.setAccessible(true);
                    field.set(socket.getInetAddress(), host);
                } catch (Exception ignored) {
                }
            }
        };
        client.getConnectionManager().getSchemeRegistry().register(new Scheme("https", socketFactory, 443));
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
