package com.adawolfs.webServices;

import android.app.Activity;
import android.util.Log;

import com.adawolfs.webServices.helpers.JsonResponse;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by adawolfs on 29/04/16.
 */
public class MainService {
    //public static String baseUrl = "https://college-backend-adawolfs.c9users.io/";
    public static String baseUrl = "http://192.168.1.61:9000/";
    //public static String baseUrl = "http://192.168.1.56:9000/";

    protected static final String TAG = "MainService";

    /** General Tags */
    protected static final String TAG_DEVICE = "device";
    protected static final String TAG_TOKEN = "token";
    protected static final String TAG_USER = "user";
    protected static final String TAG_ROLE = "role";
    protected static final String TAG_ERROR = "error";

    protected static final String DEVICE = "android";
    protected static String VERSION = "";

    protected static String token = null;
    protected static String user = null;
    protected static String role = null;
    protected static String errorMessage;
    protected static boolean failedConnection, flag;

    protected static List<Activity> pilaActividades = new ArrayList<Activity>();

    public static void initParameters(){
        token = null;
        errorMessage = null;
        failedConnection = false;
    }

    protected static void setToken(String token){
        MainService.token = token;
    }

    public static String getToken(){
        return token;
    }

    protected static void setUser(String user) {MainService.user = user;}

    public static String getUser() {return user;}

    public static String getRole() {
        return role;
    }

    public static void setRole(String role) {
        MainService.role = role;
    }

    protected static JSONObject postRequest(String url, Map<String, String> params) {
        try {
            Log.i("MainService", baseUrl + url);
            HttpPost post = new HttpPost(baseUrl + url);
            // add header
            //post.setHeader("User-Agent", USER_AGENT);

            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            for (String key : params.keySet()) {
                urlParameters.add(new BasicNameValuePair(key, params.get(key)));
            }
            if(getToken() != null){
                urlParameters.add(new BasicNameValuePair(TAG_TOKEN, getToken()));
            }
            if(getUser() != null) {
                urlParameters.add(new BasicNameValuePair(TAG_USER, getUser()));
            }
            post.setEntity(new UrlEncodedFormEntity(urlParameters));

            return JsonResponse.getJsonObject(post);

        } catch (Exception e){
            Log.e(TAG, e.getMessage());
            System.err.print(e);
        }
        return null;
    }

    protected static Object getRequest(String url, String params, Boolean string) {
        try {
            Log.i("MainService", baseUrl + url);
            StringBuilder requestUrl = new StringBuilder(baseUrl + url);
            requestUrl.append(params);

            HttpGet get = new HttpGet(requestUrl.toString());
            if (string){
                return JsonResponse.getString(get);
            }
            return JsonResponse.getJsonObject(get);

        } catch (Exception e){
            Log.e(TAG, e.getMessage());
            System.err.print(e);
        }
        return null;
    }
}

