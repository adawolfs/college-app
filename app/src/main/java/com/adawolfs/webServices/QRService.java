package com.adawolfs.webServices;

import android.util.Log;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by adawolfs on 5/9/16.
 */
public class QRService extends MainService {
    static String url = "api/QR";
    private static String message = "";
    public static void send(String qr){
        Map<String, String> params = new HashMap<String, String>();
        params.put("qr", qr);
        JSONObject json = postRequest(url, params);
        try {
            Log.i("QR", json.toString());
            setMessage(json.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getMessage(){
        return message;
    }

    public static void setMessage(String m){
        message = m;
    }
}
