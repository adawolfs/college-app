package com.adawolfs.webServices;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by adawolfs on 4/28/16.
 */
public class LoginService extends MainService {
    static String url = "loginApp";

    public static void login(String alias, String password) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("alias", alias);
        params.put("password", password);
        JSONObject json = postRequest(url, params);
        try {
            setToken(json.getString(TAG_TOKEN));
            setUser(json.getString(TAG_USER));
            setRole(json.getString(TAG_ROLE));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
