package com.adawolfs.college;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.adawolfs.qreader.QReader;
import com.adawolfs.webServices.MainService;
import com.adawolfs.webServices.StudentService;

public class TutorMenuActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutor_menu);
    }

    public void locale(View view){
        studentList(StudentService.LOCATE);
    }

    public void attend(View view){
        studentList(StudentService.ATTEND);
    }

    private void studentList(String option){
        if(MainService.getToken() != null){
            Intent i = new Intent().setComponent(new ComponentName(this, StudentListActivity.class));
            i.putExtra("option", option);
            startActivity(i);
        }
    }

    @Override
    protected void solicitud() {

    }
}
