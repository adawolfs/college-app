package com.adawolfs.college;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by adawolfs on 2/05/16.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected static ProgressDialog dialog;
    protected static boolean ready;
    private static Handler mHandler;

    protected void showProgressBar(Thread thread, Context context){
        dialog = ProgressDialog.show(context, "", "Procesando");
        thread.start();
        mHandler = new Handler();
        mHandler.removeCallbacks(stopDialog);
        mHandler.postDelayed(stopDialog, 500);
    }

    private Runnable stopDialog = new Runnable(){
        public void run(){
            if(!ready){
                mHandler.postDelayed(stopDialog, 100);
                Log.i("BASE ACTIVITY","NOT READY");
            }else{
                dialog.cancel();
                Log.i("BASE ACTIVITY","READY");
                solicitud();
            }
        }
    };

    protected abstract void solicitud();

}
