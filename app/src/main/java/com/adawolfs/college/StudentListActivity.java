package com.adawolfs.college;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.adawolfs.adapters.HistoryAdapter;
import com.adawolfs.adapters.HistoryItem;
import com.adawolfs.adapters.Student;
import com.adawolfs.adapters.StudentListAdapter;
import com.adawolfs.webServices.AttendService;
import com.adawolfs.webServices.StudentService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StudentListActivity extends BaseActivity {
    ArrayList<String> listItems = new ArrayList<>();
    ArrayAdapter<String> adapter;
    String json = null;
    List<Student> items = new ArrayList<>();
    ListView listView;
    private String option;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            option = extras.getString("option");
            Toast.makeText(this, option, Toast.LENGTH_SHORT).show();
        }
        init();
    }

    private void init() {
        listView = (ListView) findViewById(R.id.listView);
        Thread thread = new Thread() {
            public void run() {
                ready = false;
                StudentService.setOperation(option);
                json = StudentService.getStudents();
                ready = true;
            }
        };
        showProgressBar(thread, StudentListActivity.this);
    }

    @Override
    protected void solicitud() {
        Log.i("history","solicitud");
        ObjectMapper mapper = new ObjectMapper();
        try {
            items = mapper.readValue(json, new TypeReference<List<Student>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        StudentListAdapter adapter = new StudentListAdapter(this, items);
        listView.setAdapter(adapter);

    }
}
