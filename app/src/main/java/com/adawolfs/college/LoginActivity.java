package com.adawolfs.college;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.adawolfs.services.LocateService;
import com.adawolfs.webServices.LoginService;
import com.adawolfs.webServices.MainService;

public class LoginActivity extends BaseActivity {
    private Button cancel, login;
    private TextView user, password;
    private String TAG = "LoginActivity";
    private LocateService s;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }
/*
    private void getLastBestLocation() {
        LocationManager mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            System.out.println("Location return");
            return;
        }
        Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        long GPSLocationTime = 0;
        if (null != locationGPS) { GPSLocationTime = locationGPS.getTime(); }

        long NetLocationTime = 0;

        if (null != locationNet) {
            NetLocationTime = locationNet.getTime();
        }
        Location loc;
        if ( 0 < GPSLocationTime - NetLocationTime ) {
            loc = locationGPS;
        }
        else {
            loc = locationNet;
        }
        loc.getLatitude();
        loc.getLongitude();
        String text = "Mi ubicacion actual es: " + "\n Lat = "
                + loc.getLatitude() + "\n Long = " + loc.getLongitude();
        System.out.println(text);
        Toast.makeText(
                getBaseContext(),
                text, Toast.LENGTH_SHORT).show();
    }
*/
    private void init() {
        user = (TextView) findViewById(R.id.user);
        password = (TextView) findViewById(R.id.password);
        login = (Button) findViewById(R.id.login);
        cancel = (Button) findViewById(R.id.cancel);
    }

    public void cancel(View view){
        finish();
    }



    public void login(View view) {
        System.out.println("login click");
        Thread thread = new Thread() {
            public void run() {
                ready = false;
                LoginService.login(user.getText().toString(), password.getText().toString());
                ready = true;
            }
        };
        showProgressBar(thread, LoginActivity.this);
        Log.i(TAG, "Login finished");
    }

    @Override
    protected void onStop() {
        Log.i("Login", "stop");

        super.onStop();
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.i("Login", "OnResume");
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className,
                                       IBinder binder) {
            LocateService.MyBinder b = (LocateService.MyBinder) binder;
            s = b.getService();
            Toast.makeText(LoginActivity.this, "Connected", Toast.LENGTH_SHORT)
                    .show();
        }

        public void onServiceDisconnected(ComponentName className) {
            s = null;
        }
    };

    @Override
    protected void solicitud() {
        Log.i("Login","solicitud");
        if(MainService.getToken() != null && MainService.getRole() != null){
            String role = MainService.getRole();
            if(role.equals("guardian")){
                startActivity(new Intent().setComponent(new ComponentName(this, TutorMenuActivity.class)));
            } else if (role.equals("student")){
                startActivity(new Intent().setComponent(new ComponentName(this, StudentMenuActivity.class)));
            }
            finish();
        }
    }
}
