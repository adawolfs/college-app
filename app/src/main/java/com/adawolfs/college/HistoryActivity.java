package com.adawolfs.college;

import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.adawolfs.adapters.HistoryAdapter;
import com.adawolfs.adapters.HistoryItem;
import com.adawolfs.webServices.AttendService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.widget.ListView;
import android.widget.Toast;

public class HistoryActivity extends BaseActivity {
    String json = null;
    List<HistoryItem> items = new ArrayList<>();
    ListView listView;
    String user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            user = extras.getString("user");
        }
        init();
    }

    private void init() {
        listView = (ListView) findViewById(R.id.listView);
        Thread thread = new Thread() {
            public void run() {
                ready = false;
                json = AttendService.getHistorial(user);
                ready = true;
            }
        };
        showProgressBar(thread, HistoryActivity.this);
    }

    @Override
    protected void solicitud() {
        Log.i("history","solicitud");
        ObjectMapper mapper = new ObjectMapper();
        try {
           items = mapper.readValue(json, new TypeReference<List<HistoryItem>>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        HistoryAdapter adapter = new HistoryAdapter(this, items);
        listView.setAdapter(adapter);

    }
}

