package com.adawolfs.college;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.adawolfs.adapters.Location;
import com.adawolfs.webServices.LocationService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;

public class LocationActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    public String user;
    LatLng student;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            user = extras.getString("user");
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        GetLocation locationTask = new GetLocation();
        locationTask.execute((Void) null);
        // Add a marker in Sydney and move the camera

    }

    public class GetLocation extends AsyncTask<Void, Void, Boolean> {

        GetLocation() {
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            String json = LocationService.getLocation(user);
            ObjectMapper mapper = new ObjectMapper();
            try {
                Location location  = mapper.readValue(json, new TypeReference<Location>(){});
                System.out.println("Maps");
                System.out.println(location.getLatitude());
                System.out.println(location.getLongitude());
                student = new LatLng(Double.valueOf(location.getLatitude()), Double.valueOf(location.getLongitude()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mMap.addMarker(new MarkerOptions().position(student).title("User"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(student));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(student, 12.0f));

        }

        @Override
        protected void onCancelled() {
            // mAuthTask = null;
        }
    }
}
