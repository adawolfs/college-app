package com.adawolfs.college;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.adawolfs.qreader.QReader;
import com.adawolfs.services.LocateService;
import com.adawolfs.webServices.MainService;

public class StudentMenuActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_menu);
        Log.i("StudentMenu", "Before run receiver");

        Intent startIntent = new Intent(getApplicationContext(), LocateService.class);
        getApplicationContext().startService(startIntent);
        Log.i("StudentMenu", "after run receiver");
    }

    public void readQR(View view){
        Intent intent = new Intent(this, QReader.class);
        startActivity(intent);
    }

    public void getHistory(View view){
        if(MainService.getToken() != null){
            Intent i = new Intent().setComponent(new ComponentName(this, HistoryActivity.class));
            i.putExtra("user", MainService.getUser());
            startActivity(i);
        }
    }

    @Override
    protected void solicitud() {

    }
}
