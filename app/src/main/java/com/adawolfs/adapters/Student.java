package com.adawolfs.adapters;

/**
 * Created by adawolfs on 2/06/16.
 */
public class Student {
    private String _id;
    private String name;

    public Student(){

    }

    public Student(String name, String _id) {
        this.name = name;
        this._id = _id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setNamme(String namme) {
        this.name = namme;
    }
}
