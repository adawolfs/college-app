package com.adawolfs.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adawolfs.college.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adawolfs on 1/06/16.
 */
public class HistoryAdapter extends ArrayAdapter<HistoryItem> {
    Context context;
    int layout;
    private LayoutInflater inflater = null;
    List<HistoryItem> items = new ArrayList<>();
    public HistoryAdapter(Context context, List<HistoryItem> items){
        super(context, -1, items);
        this.context = context;
        this.items = items;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

//http://www.vogella.com/tutorials/AndroidListView/article.html#androidlists_adapterintro
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(R.layout.history_item, parent, false);
        HistoryItem item = items.get(position);
        TextView course = (TextView) row.findViewById(R.id.course);
        TextView date = (TextView) row.findViewById(R.id.date);

        course.setText(item.getCourse().getName());
        date.setText(item.getDate());

        return row;
    }
}
