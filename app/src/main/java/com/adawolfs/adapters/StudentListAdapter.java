package com.adawolfs.adapters;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.adawolfs.college.HistoryActivity;
import com.adawolfs.college.LocationActivity;
import com.adawolfs.college.R;
import com.adawolfs.webServices.StudentService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adawolfs on 1/06/16.
 */
public class StudentListAdapter extends ArrayAdapter<Student> {
    Context context;
    int layout;
    private LayoutInflater inflater = null;
    List<Student> items = new ArrayList<>();
    public StudentListAdapter(Context context, List<Student> items){
        super(context, -1, items);
        this.context = context;
        this.items = items;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

//http://www.vogella.com/tutorials/AndroidListView/article.html#androidlists_adapterintro
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(R.layout.student_item, parent, false);
        final Student item = items.get(position);
        TextView name = (TextView) row.findViewById(R.id.name);
        name.setText(item.getName());
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, StudentService.getOperation() + " : " + item.get_id(), Toast.LENGTH_SHORT).show();
                if(StudentService.ATTEND.equals(StudentService.getOperation())){
                    Intent i = new Intent().setComponent(new ComponentName(context, HistoryActivity.class));
                    i.putExtra("user", item.get_id());
                    context.startActivity(i);
                } else if (StudentService.LOCATE.equals(StudentService.getOperation())){
                    Intent i = new Intent().setComponent(new ComponentName(context, LocationActivity.class));
                    i.putExtra("user", item.get_id());
                    context.startActivity(i);
                }
            }
        });
        return row;
    }
}
