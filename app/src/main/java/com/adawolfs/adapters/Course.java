package com.adawolfs.adapters;

/**
 * Created by adawolfs on 2/06/16.
 */
public class Course {
    private String _id;
    private String name;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setNamme(String namme) {
        this.name = namme;
    }
}
