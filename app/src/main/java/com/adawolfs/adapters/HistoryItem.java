package com.adawolfs.adapters;

/**
 * Created by adawolfs on 1/06/16.
 */
public class HistoryItem {
    public Course course;
    private String _id;
    public String date;

    public HistoryItem(){

    }

    public HistoryItem(Course course, String date, String _id) {
        super();
        this.course = course;
        this.date = date;
        this._id = _id;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }


}
