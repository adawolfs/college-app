package com.adawolfs.adapters;

/**
 * Created by adawolfs on 8/06/16.
 */
public class Location {
    private String _id;
    private String latitude;
    private String longitude;
    private String date;

    public Location() {
    }

    public Location(String _id, String latitude, String longitude, String date) {
        this._id = _id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
